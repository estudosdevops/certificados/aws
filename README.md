# AWS Certificados

Estas são minhas anotações dos cursos da `Udemy` para os certificados abaixo.
- [AWS Certified Solutions Architect - Associate 2019](https://www.udemy.com/aws-certified-solutions-architect-associate/ "Udemy | AWS Certified Solutions Architect")
- [AWS Certified SysOps Administrator - Associate 2019](https://www.udemy.com/aws-certified-sysops-administrator-associate/ "Udemy| AWS Certified SysOps Administrator")

O conteúdo dos cursos segue o mesmo padrão da `Cloud Guru`, segue abaixo links dos cursos
- [AWS Certified Solutions Architect - Associate 2019](https://acloud.guru/learn/aws-certified-solutions-architect-associate "Cloud Guru | AWS Certified Solutions Architect")
- [AWS Certified SysOps Administrator - Associate 2019](https://acloud.guru/learn/aws-certified-sysops-administrator-associate-2019 "Cloud Guru | AWS Certified SysOps Administrator")

## Sobre os exames AWS

- Duração do exame: `130m`
- Quantas Perguntas: `60`
  - às vezes isso pode mudar
- Multipla Escolha: `Sim`
- Porcentagem para passar: `70%`
- Perguntas baseadas em cenários: `Sim`

## AWS Certified Solutions Architect

- [0K_Overview](https://gitlab.com/estudosdevops/certificados/aws/tree/master/Architect/10k_overview "10K_Overview")
-  [IAM Identity Access Management](https://github.com/AlessioCasco/AWS-CSA-2019-study-notes/blob/master/Course/IAM/README.md "IAM Identity Access Management")
-  [Amazon S3](https://github.com/AlessioCasco/AWS-CSA-2019-study-notes/tree/master/Course/S3 "Amazon S3")

## Referência

- [AWS Certified Solutions Architect - Associate 2019](https://github.com/AlessioCasco/AWS-CSA-2019-study-notes "Github")
